package auth

import (
	"fmt"
	"net/http"
	"os"
	"strings"

	"github.com/dgrijalva/jwt-go"
)

func CheckJWT(jwtstring string) (*jwt.Claims, error) {
	// users := users.UserClaim{}

	tokens, err := jwt.Parse(jwtstring, func(token *jwt.Token) (interface{}, error) {
		if method, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("Signing method Invalid")
		} else if method != jwt.SigningMethodHS256 {
			return nil, fmt.Errorf("Signing method Invalid")
		}
		return []byte(os.Getenv("SALTKEY")), nil
	})
	if err != nil {
		return nil, err
	}
	claims := tokens.Claims
	// if !ok || !tokens.Valid {
	// 	return  err, nil
	// }
	if claims == nil {
		return nil, fmt.Errorf("Claims nill")
	}
	// b, _ := json.Marshal(claims)
	// json.Unmarshal(b, &users)
	return &claims, nil
}

func OnlyParse(jwtstring string) (*jwt.Claims, error) {
	tokens, _ := jwt.Parse(jwtstring, nil)
	if tokens == nil {
		return nil, fmt.Errorf("Failed to parse")
	}
	claims := tokens.Claims
	return &claims, nil

}

// GetTokenHeader for get token header from request
func GetTokenHeader(r *http.Request) (token string, status int, err error) {
	authHeader := r.Header.Get("Authorization")
	if !strings.Contains(authHeader, "Bearer") {
		status = http.StatusUnauthorized
		err = fmt.Errorf("Auth header is not bearer type")
		return
	}
	splitToken := strings.Split(authHeader, "Bearer")
	if len(splitToken) != 2 {
		status = http.StatusUnauthorized
		err = fmt.Errorf("Auth header format mismatch")
		return
	}
	body := strings.Split(splitToken[1], ".")
	if len(body) < 2 {
		status = http.StatusUnauthorized
		err = fmt.Errorf("Error format of jwt")
		return
	}
	token, err = Decrypt(body[1])
	if err != nil {
		status = http.StatusInternalServerError
		err = fmt.Errorf("Internal Server Error :%s", err)
		return
	}

	body[1] = token
	token = strings.Join(body, ".")
	token = strings.Trim(token, " ")
	return
}
