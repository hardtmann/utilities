package auth

import (
	"errors"
	"log"
	"os"
	"strconv"
	"strings"
	"time"

	jwt "github.com/dgrijalva/jwt-go"
)

var (
	TokenDuration = 24
)

type TokenClaims struct {
	jwt.StandardClaims
	TokenData map[string]interface{} `json:"token_data"`
}

/*
	Data will pass using map[string]interface{} and token will be saved inside redis cache, you can access it manualy
*/

func GenerateToken(data map[string]interface{}) (*time.Time, string, error) {
	if os.Getenv("TOKEN_EXPIRE") != "" {
		var err error
		TokenDuration, err = strconv.Atoi(os.Getenv("TOKEN_EXPIRE"))
		if err != nil {
			log.Print(err)
			TokenDuration = 24
		}
	}

	claims := TokenClaims{
		StandardClaims: jwt.StandardClaims{
			Issuer:    os.Getenv("APP_NAME"),
			ExpiresAt: time.Now().Add(time.Duration(TokenDuration) * time.Hour).Unix(),
		},
		TokenData: data,
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

	if os.Getenv("SALTKEY") == "" {
		return nil, "", errors.New("Server Saltkey is not defined")
	}

	signedToken, err := token.SignedString([]byte(os.Getenv("SALTKEY")))
	if err != nil {
		return nil, "", err
	}

	signedToken, err = extractBody(signedToken)
	if err != nil {
		return nil, "", err
	}

	expireOn := time.Now().Add(time.Duration(TokenDuration) * time.Hour)
	return &expireOn, signedToken, nil
}

func extractBody(signedToken string) (string, error) {
	var err error
	body := strings.Split(signedToken, ".")
	signedToken, err = Encrypt(body[1])
	body[1] = signedToken
	signedToken = strings.Join(body, ".")
	return signedToken, err
}
