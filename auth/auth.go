package auth

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	"gitlab.com/hardtmann/utilities/utils"
)

// Authenticate ...
func Authenticate(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		token, stat, err := GetTokenHeader(r)
		if err != nil {
			log.Println("error get token header")
			utils.ResponseRender("error", w, stat, err)
			return
		}

		claims, err := CheckJWT(token)
		if err != nil {
			log.Println("error signing token")
			utils.ResponseRender("error", w, 440, fmt.Errorf("error while signing tokens!:%s", err))
			return
		}

		b, err := json.Marshal(claims)
		if err != nil {
			log.Println("error formatting token")
			utils.ResponseRender("error", w, 500, fmt.Errorf("error formatting tokens:%s", err))
			return
		}

		requestHead := TokenClaims{}
		err = json.Unmarshal(b, &requestHead)
		ctx := context.WithValue(r.Context(), "token_data", requestHead) //all context data that contains in token
		h.ServeHTTP(w, r.WithContext(ctx))
	})
}
