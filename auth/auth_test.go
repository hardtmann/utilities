package auth_test

import (
	"log"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"

	"github.com/gorilla/mux"
	"github.com/stretchr/testify/assert"
	. "gitlab.com/hardtmann/utilities/auth"
)

func SetToken(req *http.Request, token string) {
	bearer := "Bearer " + token
	req.Header.Add("Authorization", bearer)
}

func TestAuthenticate(t *testing.T) {
	os.Setenv("SALTKEY", "B2I2A2E4l2432238")
	defer os.Unsetenv("SALTKEY")

	_, token, err := GenerateToken(map[string]interface{}{"test": "test"})
	assert.NoError(t, err)

	req, err := http.NewRequest("GET", "/", nil)
	if err != nil {
		t.Fatal(err)
	}

	SetToken(req, token)

	rr := httptest.NewRecorder()

	r := mux.NewRouter()
	r.Use(Authenticate)
	r.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		log.Println("yes")
	})
	r.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusOK {
		t.Errorf("Handler returned wrong status code: got %v want %v: %v",
			status, http.StatusOK, rr.Body.String())
	}

	log.Println(rr.Body.String())
}
