package middlewares

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"

	jwt "gitlab.com/hardtmann/utilities/auth"
	"gitlab.com/hardtmann/utilities/utils"
)

// Authenticate ...
func Authenticate(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		token, stat, err := jwt.GetTokenHeader(r)
		if err != nil {
			utils.ResponseRender("error", w, stat, err)
			return
		}

		claims, err := jwt.CheckJWT(token)
		if err != nil {
			utils.ResponseRender("error", w, 440, fmt.Errorf("error while signing tokens!:%s", err))
			return
		}

		b, err := json.Marshal(claims)
		if err != nil {
			utils.ResponseRender("error", w, 500, fmt.Errorf("error formatting tokens:%s", err))
			return
		}

		requestHead := jwt.TokenClaims{}
		err = json.Unmarshal(b, &requestHead)
		ctx := context.WithValue(r.Context(), "tokens_data", requestHead) //all context data that contains in token
		h.ServeHTTP(w, r.WithContext(ctx))
	})
}
