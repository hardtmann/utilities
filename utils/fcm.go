package utils

import (
	"bytes"
	"encoding/json"
	"errors"
	"io/ioutil"
	"log"
	"net/http"
	"os"
)

type FCMPayload struct {
	Priority        string              `json:"priority"`
	RegistrationIDs []string            `json:"registration_ids"`
	Notification    FCMNotification     `json:"notification"`
	PackageName     string              `json:"restricted_package_name"`
	Data            map[string]string   `json:"data"`
	APNS            FCMAPNSNotification `json:"apns"`
}

// FCMNotification ...
type FCMNotification struct {
	Title       string `json:"title"`
	Body        string `json:"body"`
	Sound       string `json:"sound"`
	ClickAction string `json:"click_action"`
	Color       string `json:"color"`
	//RestrictedPackagename string `json:"restricted_package_name"`
	Priority string `json:"priority"`
}

// FCMAPNSNotification ..
type FCMAPNSNotification struct {
	Headers FCMAPNSHeaders `json:"headers"`
	Payload FCMAPNSPayload `json:"payload"`
}

// FCMAPNSPayload ..
type FCMAPNSPayload struct {
	APS FCMAps `json:"aps"`
}

// FCMAps ..
type FCMAps struct {
	Alert string `json:"alert"`
	Sound string `json:"sound"`
}

// FCMAPNSHeaders ..
type FCMAPNSHeaders struct {
	Priority string `json:"apns-priority"`
}

func SendFCMPushNotification(fcmID []string, title, body, clickAction, packageName string, sound string, data map[string]string) error {
	fcmPayload := FCMPayload{
		Priority:        "high",
		Data:            data,
		RegistrationIDs: fcmID,
		Notification: FCMNotification{
			Priority:    "high",
			Title:       title,
			Body:        body,
			Color:       "#0000FF",
			ClickAction: clickAction,
			Sound:       sound,
		}, //	PackageName: packageName,

		APNS: FCMAPNSNotification{
			Headers: FCMAPNSHeaders{
				Priority: "10",
			},
			Payload: FCMAPNSPayload{
				APS: FCMAps{
					Alert: "",
					Sound: "default",
				},
			},
		},
	}
	jsonMarshal, _ := json.Marshal(fcmPayload)
	fcmKey := os.Getenv("FCM_KEY")
	fcmURL := "https://fcm.googleapis.com/fcm/send"
	req, err := http.NewRequest("POST", fcmURL, bytes.NewBuffer(jsonMarshal))
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", "key="+fcmKey)
	if err != nil {
		log.Println(err.Error())
		return err
	}

	hc := http.Client{}
	response, err := hc.Do(req)
	if err != nil {
		log.Println("[FCM] Error" + err.Error())
		return err
	}
	if response.StatusCode != 200 {
		b, _ := ioutil.ReadAll(response.Body)
		log.Println(string(b))
		return errors.New("FCM Error")
	}
	resp, err := ioutil.ReadAll(response.Body)
	if err != nil {
		log.Println(err.Error())
	}
	log.Println("[FCM] Success " + string(resp))
	return nil
}
