package utils

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"runtime"
	"time"
)

type (
	//customResponseWriter ...
	customResponseWriter struct {
		http.ResponseWriter
		Request    json.RawMessage
		Response   interface{}
		StatusCode int
	}
	//Default middleware
	Default struct{}
)

type LogFormat struct {
	Timestamp  time.Time   `json:"timestamp"`
	HTTPMethod string      `json:"method"`
	URL        string      `json:"url"`
	Host       string      `json:"host"`
	Status     int         `json:"status"`
	Response   interface{} `json:"response"`
	Request    interface{} `json:"request"`
	Message    string      `json:"message"` //message for stackdriver
}

//LogAndRecovery is var responsible for extension from this package
var LogAndRecovery Default

//WriteHeader is a function that overiding default WriteHeader
func (w *customResponseWriter) WriteHeader(code int) {
	w.StatusCode = code
	w.ResponseWriter.WriteHeader(code)
}

func (w *customResponseWriter) Write(data []byte) (int, error) {
	_ = json.Unmarshal(data, &w.Response)

	return w.ResponseWriter.Write(data)
}

func (d *Default) DefaultLog(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		defer RecoveryFromPanic(w, r)

		// skip logging health checks
		if r.URL.RequestURI() == "/health" {
			h.ServeHTTP(w, r)
		} else {
			lw := &customResponseWriter{
				ResponseWriter: w,
			}

			logs := ExtractRequest(r)
			h.ServeHTTP(lw, r)
			logs.Status = lw.StatusCode
			logs.Response = lw.Response
			b, _ := json.Marshal(logs)
			fmt.Printf("%v", string(b))
		}
	})
}

func RecoveryFromPanic(w http.ResponseWriter, r *http.Request) {
	if rec := recover(); rec != nil {
		var file []string
		i := 0
		for {
			_, tempFile, tempLine, status := runtime.Caller(i)
			file = append(file, tempFile+" : "+fmt.Sprint(tempLine))
			if !status {
				break
			}
			i++
		}
		logs := ExtractRequest(r)
		logs.Response = map[string]interface{}{
			"error": fmt.Sprint(rec),
			"trace": file,
		}
		log.Print(rec)
		b, err := json.Marshal(logs)
		if err != nil {
			log.Print(err)
		}
		fmt.Print(string(b))

		ResponseRender("error", w, http.StatusInternalServerError, "Internal Server Error: Panic Recovery Called")
		return
	}
}

//ExtractRequest ..
func ExtractRequest(r *http.Request) LogFormat {
	defer r.Body.Close()
	buff, err := ioutil.ReadAll(r.Body)
	request1 := ioutil.NopCloser(bytes.NewBuffer(buff))
	requestBody := ioutil.NopCloser(bytes.NewBuffer(buff))
	r.Body = request1
	if err != nil {
		log.Print(err)
	}
	decoder := json.NewDecoder(requestBody)

	var temp map[string]interface{}
	_ = decoder.Decode(&temp)
	host, _ := os.Hostname()
	url := r.URL.RequestURI()
	return LogFormat{
		Timestamp:  time.Now().Local(),
		Host:       host,
		HTTPMethod: r.Method,
		URL:        url,
		Message:    "API Log for url : " + url,
		Request: map[string]interface{}{
			"query":  r.URL.Query(),
			"body":   temp,
			"header": r.Header,
		},
	}
}
