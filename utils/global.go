package utils

import (
	"fmt"
	"net/http"

	"github.com/thedevsaddam/renderer"
)

var Rnd = renderer.New()

// Version : describes standard rendered JSON for Apps Versioning
type Version struct {
	Code string `json:"code"`
	Name string `json:"name"`
}

// Standard describes standard rendered JSON for Standard Response
type Standard struct {
	RequestParam string      `json:"request_param"`
	Status       string      `json:"status"`
	ErrorMessage string      `json:"error_message"`
	Data         interface{} `json:"data"`
	Next         string      `json:"next"`
	Version      Version     `json:"version"`
}

func ResponseRender(types string, w http.ResponseWriter, statusCode int, v interface{}) {
	if types == "error" {
		responseData := Standard{
			RequestParam: "",
			Status:       "error",
			ErrorMessage: fmt.Sprintf("%v", v),
			Data:         nil,
			Next:         "",
			Version:      Version{Code: "1", Name: "0.1.0"},
		}
		Rnd.JSON(w, statusCode, responseData)
	} else {
		responseData := Standard{
			RequestParam: "",
			Status:       "success",
			ErrorMessage: "",
			Data:         v,
			Next:         "",
			Version:      Version{Code: "1", Name: "0.1.0"},
		}
		Rnd.JSON(w, statusCode, responseData)
	}
}

// CheckHealth ...
func CheckHealth(w http.ResponseWriter, r *http.Request) {
	ResponseRender("success", w, http.StatusOK, "health")
	return
}
